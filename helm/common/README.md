# Чарт общей библиотеки

## Параметры

Описание методов этой библиотеки сгруппированное в таблицы по областям применения.

### Наименования

Идентификатор | Описание | Использование
--- | --- | ---
`common.names.name` | Наименование из чарта или `.Values.nameOverride` | `.` Контекст чарта
`common.names.fullname` | Полное наименования | `.` Контекст чарта
`common.names.chart` | Наименование и версия чарта | `.` Контекст чарта

### Образы

Идентификатор | Описание | Использование
--- | --- | ---
`common.images.image` | Полное наименование образа | `dict "imageRoot" .Values.path.to.the.image "global" $`, см. [ImageRoot](#imageroot).

### Шаблонизированные значения

Идентификатор | Описание | Использование
--- | --- | ---
`common.tplvalues.render` | Рендеринг значения в котором содержится шаблон | `dict "value" .Values.path.to.the.Value "context" $`

## Схемы

### ImageRoot

```yaml
registry:
  type: string
  description: Docker Registry, в котором размещен образ
  example: docker.io

repository:
  type: string
  description: Репозиторий и наименование образа
  example: company/foo

tag:
  type: string
  description: Метка образа
  example: 1.0.0
```
