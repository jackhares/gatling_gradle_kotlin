{{/*
Лейблы для шаблона для istio gateways
*/}}
{{- define "common.istio.labels.gateway" -}}
{{ include "common.istio.labels.matchLabels" . }}
helm.sh/chart: {{ include "common.names.chart" . }}
app.kubernetes.io/name: {{ include "common.names.name" . }}
app.kubernetes.io/component: gateways
app.kubernetes.io/instance: {{ required "A valid .Values.istio.controlPlane entry required!" .Values.istio.controlPlane }}
app.kubernetes.io/managed-by: maistra-istio-operator
app.kubernetes.io/part-of: istio
{{- if .Values.istio.rhsm20.enabled }}
app.kubernetes.io/version: 2.0.2-3.el8-76
maistra-version: 2.0.2
{{- else }}
app.kubernetes.io/version: 1.0.2-7.el8-1
{{- end }}
chart: gateways
heritage: Tiller
maistra.io/owner: {{ required "A valid .Values.istio.controlPlane entry required!" .Values.istio.controlPlane }}
release: istio
{{- end -}}

{{/*
Labels общая часть 
*/}}
{{- define "common.istio.labels" -}}
{{ include "common.istio.labels.matchLabels" . }}
{{ include "common.labels.standard" . }}
{{- end -}}mp

{{/*
Labels to use on deploy.spec.selector.matchLabels and svc.spec.selector
*/}}
{{- define "common.istio.labels.matchLabels" -}}
app: {{ .Values.istio.gateway }}
istio: {{ .Values.istio.gateway }}
{{- end -}}

{{/* vim: set filetype=mustache: */}}
{{/*
Kubernetes standard labels
*/}}
{{- define "common.labels.standard" -}}
app.kubernetes.io/name: {{ include "common.names.name" . }}
helm.sh/chart: {{ include "common.names.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Labels to use on deploy.spec.selector.matchLabels and svc.spec.selector
*/}}
{{- define "common.labels.matchLabels" -}}
app.kubernetes.io/name: {{ include "common.names.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}