{{/*
Возвращает contextPath. Добавляет "/" к началу context
*/}}
{{- define "common.spring.contextPath" -}}
{{ $separator := "/" }}
{{- if .Values.contextPath -}}
  {{- if eq .Values.contextPath $separator -}}
{{ $separator | replace "/" "" }}
  {{- else -}}
{{ printf "%s%s" $separator (.Values.contextPath | replace "/" "") }}
  {{- end -}}
{{- else -}}
{{ printf "%s%s" $separator .Release.Name }}
{{- end -}}
{{- end -}}

{{/*
Возвращает наименование ConfigMap для приложения
*/}}
{{- define "common.spring.app.configMapName" -}}
{{- printf "%s-app" .Release.Name -}}
{{- end -}}

{{/*
Возвращает наименование Secret для приложения
*/}}
{{- define "common.spring.app.secretName" -}}
{{- printf "%s-app" .Release.Name -}}
{{- end -}}

{{/*
Возвращает наименование ConfigMap для JVM
*/}}
{{- define "common.spring.jvm.configMapName" -}}
{{- printf "%s-jvm" .Release.Name -}}
{{- end -}}
