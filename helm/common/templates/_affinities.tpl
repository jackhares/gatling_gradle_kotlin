{{- define "common.affinities.pods" -}}
requiredDuringSchedulingIgnoredDuringExecution:
    - labelSelector:
        matchLabels: {{- (include "common.labels.matchLabels" .) | nindent 10 }}
      topologyKey: kubernetes.io/hostname
{{- end -}}