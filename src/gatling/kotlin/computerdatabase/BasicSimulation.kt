package computerdatabase

import 	io.gatling.javaapi.core.*

import io.gatling.javaapi.core.CoreDsl.*
import io.gatling.javaapi.http.HttpDsl.*

class BasicSimulation : Simulation() {

	private val addComputer = ""

	val httpProtocol = http
		.baseUrl("http://localhost:8080")

	val scenario = scenario("Computer Scenario")
	.exec(http("getComputers")
		.get("/")
		.check(
			status().`is`(200),
		)
	)

	init {
		setUp(scenario.
		injectOpen(
			atOnceUsers(100)

		).protocols(httpProtocol))
	}



}
