package computerdatabase


import 	io.gatling.javaapi.core.*

import ch.qos.logback.classic.LoggerContext
import io.gatling.app.Gatling
import io.gatling.core.config.GatlingPropertiesBuilder
//import io.gatling.core.controller.inject.open.OpenInjectionStep
import ch.qos.logback.classic.Level
import org.slf4j.LoggerFactory

var classs = BasicSimulation::class.qualifiedName

class Debugs: Simulation() {

	init {
		setUp(BasicSimulation().scenario.
		injectOpen(
			CoreDsl.atOnceUsers(1)
		).protocols(BasicSimulation().httpProtocol))
	}

}

object Debug: Simulation() {
	@JvmStatic
	fun main(args: Array<String>) {

		var LOG_LEVEL: String = System.getenv("LOG_LEVEL") ?: "WARN"

		val context: LoggerContext = LoggerFactory.getILoggerFactory() as LoggerContext

		if (LOG_LEVEL.equals(Level.TRACE)) {
			context.getLogger("io.perf.gatling.http.engine.response").setLevel(Level.valueOf(LOG_LEVEL))
		} else if (LOG_LEVEL.equals(Level.DEBUG)) {
			context.getLogger("io.perf.gatling.http.engine.response").setLevel(Level.valueOf("DEBUG"))
		} else {
			context.getLogger("io.perf.gatling.http").setLevel(Level.valueOf("INFO"))
		}

		val props = GatlingPropertiesBuilder()
			 .simulationClass(classs.toString())
			 .resourcesDirectory(IDEPathHelper.resourcesDirectory)
			 .resultsDirectory(IDEPathHelper.resultsDirectory)
			 .binariesDirectory(IDEPathHelper.mavenBinariesDirectory)

		Gatling.fromMap(props.build())

	}
}


