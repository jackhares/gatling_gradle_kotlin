package computerdatabase

import io.gatling.recorder.GatlingRecorder
import io.gatling.recorder.config.RecorderPropertiesBuilder
import scala.Option
import java.nio.file.Paths

object Recorder {
	@JvmStatic
	fun main(args: Array<String>) {

		val props = RecorderPropertiesBuilder()
			.simulationsFolder(IDEPathHelper.resourcesDirectory.toString())
			.simulationPackage("computerdatabase")
			.resourcesFolder(IDEPathHelper.mavenBinariesDirectory.toString())

		GatlingRecorder.fromMap(props.build(), Option.apply(Paths.get(IDEPathHelper.gatlingConfUrl)))

	}


}
